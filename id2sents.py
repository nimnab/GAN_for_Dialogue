

path = '/data/users/nima/tf_seminar/OpenSubData/'


rev_vocab = []


with open (path + 'movie_25000', 'r') as f:
    rev_vocab.extend(f.readlines())
    rev_vocab = [line.strip() for line in rev_vocab]
    vocab = dict([(x, y) for (y, x) in enumerate(rev_vocab)])

query = open(path + 'long/query', 'w+')
answer = open(path + 'long/answer', 'w+')


with open (path + 't_given_s_dialogue_length2_6.txt', 'r') as f:
    for line in f:
        q = line.split('|')[1]
        a = line.split('|')[0]
        ql = q.split()
        al = a.split()
        # [print(rev_vocab[int(i)]) for i in lis]
        qlis = [rev_vocab[int(i)] for i in ql]
        alis = [rev_vocab[int(i)] for i in al]        
        query.write(' '.join(qlis)+'\n')
        answer.write(' '.join(alis)+'\n')
